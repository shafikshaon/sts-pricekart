from django.shortcuts import render

# Create your views here.
from django.views import View


class CategoryDetailsView(View):
    def get(self, request):
        return render(request, 'category/category.html')


class ProductDetailsView(View):
    def get(self, request):
        return render(request, 'category/product-details.html')
