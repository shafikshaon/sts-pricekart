from django.urls import path
from .views import CategoryDetailsView, ProductDetailsView


urlpatterns = [
    path('', CategoryDetailsView.as_view()),
    path('/product-details', ProductDetailsView.as_view()),

]
